EAC-Solr
========

Apache Solr configuration files used for basic indexing of EAC-CPF, HTML and
image content.


License
-------

Please see the LICENSE.txt file for license information.


Installation
------------

Install the `schema.xml` file into the respective Solr core folder on your 
server. Restart the web application server. Reindex data and commit changes to
Solr.


Credits
-------

EAC-Solr is a project of the eScholarship Research Center at the University
of Melbourne. For more information about the project, please contact us at:

 >  eScholarship Research Center
 > University of Melbourne
 > Parkville, Victoria
 > Australia
 > www.esrc.unimelb.edu.au

Authors:

 * Davis Marques <davis.marques@unimelb.edu.au>
 * Marco La Rosa <marco@larosa.org.au>

Thanks:

 * Apache Solr - http://lucene.apache.org/solr/
 * EAC-CPF Schemas - http://eac.staatsbibliothek-berlin.de/eac-cpf-schemas.html


Revision History
----------------

0.6.0

 * Updated schema for Solr4. Renamed older schema to identify it as Solr3 version.
 * Added new hints field to support autocomplete service

0.5.1

 * Renamed fields to better clarify their content
 * Added additional digital object proxy representation fields

0.5.0

 * Added digital object fields
 * Split inferred data fields into separate Location and Time Period, and Inferred Data sections
 * Renamed source_uri and referrer_uri to eaccpf_uri and html_uri respectively

0.4.2

 * Removed EAC related files.
 * Added additional primary_city, primary_region, primary_country fields to location

0.4.1

 * Updated EAC-CPF indexing schema
 * Added EAC-CPF to Solr Input Document transform
 * Updated EAC to Solr Input Document transform

0.4.0

 * Added fields to support spatial search
 * Changed license from Apache 2 to GNU GPL 2
 * Removed some existing fields to simplify search setup
 * Split schema into EAC and EAC-CPF specific files
 * Improved inline documentation
 * Added catch all field
 * Renamed some fields to make them more succinct

0.3.0

 * Removed all extraneous comments from configuration file
 * Added remaining fields of interest

0.2.0

 * Added omitNorms="true" to boostable fields
 * Revised field types

0.1.0

 * First version

